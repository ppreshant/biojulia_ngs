# Custom functions for easing fastq reading, writing etc.
# Prashant, 2/1/21

# Load modules
using FASTX
using BioAlignments
using BioSequences
using CodecZlib


# reads fastq if compressed (.fastq.gz) or not (.fastq)
function readfastq(
	filename::String) # path of the input fastq file

	if(occursin(".gz", filename)) # for comperessed .gz files
		FASTQ.Reader(GzipDecompressorStream(open(filename)))

	else
		open(FASTQ.Reader, filename)
	end
end



# Read the first few paired end sequences into a fasta file
function pairedfastqsubset(
	fastqpaths::Vector{String}, # [path to file1, file2]
	numberofsequences::Integer, # number of sequences to read from start
	minlength::Integer, # the minimum length to pass into output file
	outputfastapath::String) # path of output fasta file

	# Initialize variables : undef vectors (fill with random values)
	i = 1 # iterator
	k = 1 # read count
	seqlengths = Vector{Int64}(undef, numberofsequences); # sequence lengths
	seqsubset = Vector{NucSeq}(undef, numberofsequences) # sequence subset

	# open the writer stream
	fastqwriter = open(FASTA.Writer, "results/" * outputfastapath)

	# Open the reader streams
	readers = map(fastqpaths) do w
		readfastq(w)
	end

	# reader1 = open(FASTQ.Reader, fastqpath1)
	# reader2 = open(FASTQ.Reader, fastqpath2)

	record1 = FASTQ.Record() # create an empty record to store in each iteration
	record2 = FASTQ.Record()

	# iteration over all/few reads in the file
	while i < numberofsequences + 1
	# while !eof(reader) # to read the whole file

		# assign the current entry to record -> memory efficient
		read!(readers[1], record1)
		read!(readers[2], record2)

		# continue only if length is > minimum
		if seqsize(record1) >= minlength # skip to next sequence if < minimum length

			# write to the growing fasta file
			writefasta(k, "_F", record1, fastqwriter) # forward read
			writefasta(k, "_R", record2, fastqwriter) # reverse read

			# seqsubset[i] = record |> FASTQ.sequence
			seqlengths[i] = seqsize(record1) # length(FASTX.sequence(record))

			i += 1 # increment i (iterator)
		end
		k += 1 # increment read count

	end

	close(fastqwriter) # close the file writer connection
	map(close, readers) # and readers

	seqlengths # return values
end


# writes a fasta file from fastq record; use in a loop to subset from fastq
function writefasta(
	i::Int, key::String, # record number and additional string (F/R read)
	seqrecord::FASTQ.Record, # the fastq record
	writingstream::FASTA.Writer) # a writable stream of a fasta file

	write(
		writingstream,
		FASTA.Record(
					string("seq_", i, key),
					seqrecord |> FASTQ.sequence)
			)
end


# get the fastq quality along sequence for the first n sequences
function subsetqualitystrings(
	fastqpath::String, # path of the fastq file
	numberofsequences::Integer) # number of sequences to retrieve
	# outputfastapath::String) #

	# INITIALIZE undef vector of vectors to store the quality scores
	qualitylist = Vector{Vector{UInt8}}(undef, numberofsequences)
	# the innermost vector can have different lengths based on read length

	# Open the reader stream
	reader = readfastq(fastqpath)

	# Initialize variables
	i = 1 # iterator
    record = FASTQ.Record() # single dummy record holder -- overwrite each iteration

	while i < numberofsequences + 1
	#while !eof(reader)
        read!(reader, record) # assign the current entry to record --> overwrites

		qualitylist[i] = record |> FASTQ.quality

		i += 1 # increment i
        ## Do something.
	end

	close(reader) # close filestreams

	qualitylist # return the vector of quality scores
end
