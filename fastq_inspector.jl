### A Pluto.jl notebook ###
# v0.19.45

using Markdown
using InteractiveUtils

# ╔═╡ 729fd529-ddfc-4d0a-b312-3756ad7e6a92
# Activate the desired environment
begin
	using Pkg
	Pkg.activate(".") # This activates the environment of folder bio_julia
	# Pkg.status() # check that the environment is active
end

# ╔═╡ 8cb18ea8-50dd-11ec-1fa5-a9f0dceeaf77
# Load modules and custom functions
begin
	using FASTX
	using BioAlignments
	using BioSequences
	using Gadfly # for plots

	include("fastqfunctions.jl") # custom functions for fastq data
end

# ╔═╡ 8adc5b1f-2703-433c-9b78-3fe9131ccedb
# User inputs
begin
	basefilename = "m-1-U64"
	dirpath = "S090/set2/"

	# preprocessing/filtered/merged datasets
	results_dirpath = "S090-set2_U64/preprocessing/run1/" 
	numberofseqs = 4 # number of sequences to read
end

# ╔═╡ 84e07b02-86b0-4d5d-ba1f-48323b10af02
md"""
# Extract paired reads from raw `fastq.gz` files
"""

# ╔═╡ e69b6656-9323-4f07-8ec8-2f52b313f0d6
# Processed: user inputs
begin
	filename1 = basefilename * "_R1_001" # name of the fastq files to open
	filename2 = basefilename * "_R2_001"
end

# ╔═╡ 96f976aa-415f-42a1-925d-f9ff10ecdd2c
# Load file
begin
	path = "/home/prashantk/NGS/NGS_data/" * dirpath # use absolute path of the data file
	
	fastqfilepath1, fastqfilepath2 = 
		path .* 
		[filename1, filename2] .* 
		".fastq.gz" # join to make full path
	
	# fastqfilepath2 = path * filename2 * ".fastq"
end	

# ╔═╡ b4b48f33-53f1-4989-bcd6-8ed87c18de49
# listing the lengths of sequences to retrieve
begin 
		
	lengthsofinterest = [97, 162, 232]
	toplength = 232
	midlength = 162
	lowlength = 97
end

# ╔═╡ 5c330e6b-4aa3-4860-874d-b2b69d70eb47
pairedfastqsubset([fastqfilepath1, fastqfilepath2], numberofseqs, 250, "raw_" * basefilename * "_top10.fasta")

# ╔═╡ 3bc8b9e7-af20-4e22-adaf-0dc9016743c8
md"""
# Extract paired reads from processed data
"""

# ╔═╡ 46e17e31-9fde-4610-b5ac-85087487a5c5
# enter filenames of filtered
proc_filepaths = 
		"/home/prashantk/NGS/NGS_analysis/" * results_dirpath * 
		basefilename .*
		[".fwd", ".rvs"] .* 
		".fastq.gz" # join to make full path

# ╔═╡ 950dbe45-68dd-4a07-87c5-03caf3c9c8cf
# save first 10 filtered data to fasta
pairedfastqsubset(proc_filepaths, numberofseqs, 200, 
	"cut_" * basefilename * "_top" * string(numberofseqs) * ".fasta")

# ╔═╡ 03e8865b-33a3-47e7-8ffd-fdbe6e289758
md"""
## Other stuff
"""

# ╔═╡ ca4e1b48-ea73-4934-8e8b-3db5ccc7c1aa
# filename of merged data
mergedpath = "/home/prashant/NGS/NGS_analysis/output16S/merged/1/" * 
		"U64_col"

# ╔═╡ c8a54abe-af83-4816-921e-9e4222f8bc4a
# make a vector of vectors : multiple rows of column vectors?
# source : https://discourse.julialang.org/t/preallocating-vector-of-vectors/69304/17
function makearr4(::Val{T}, Nrow, Ncol) where T
           [ Vector{T}(undef, Ncol) for _ in 1:Nrow ]
       end

# ╔═╡ 4f43c2a5-49b8-4295-a262-3ccebfc0c4d9
tst = Vector{Vector{Integer}}(undef, 5)

# ╔═╡ 440f3f60-a611-4a42-be96-7a98333cff2f
qualitytraces = subsetqualitystrings(fastqfilepath1, 10)

# ╔═╡ e50466f0-863c-4ea6-927f-993838a629c3
md"""
# quality traces along the read
"""

# ╔═╡ d83273c3-ed36-4f18-b07c-a024dc9bf265
plot(y = qualitytraces[5], Geom.line, Geom.point)

# ╔═╡ 585433d8-b8ee-41c3-80d9-42080859a6cb
filteredqualtraces = subsetqualitystrings(filteredpaths[1], 10)

# ╔═╡ 5510c22f-3a17-47cb-b1f3-78192b57c56f
plot(y = filteredqualtraces[5], Geom.line, Geom.point)

# ╔═╡ c7090264-0874-422c-ab2f-cb76f4135433
qualitytraces[3] |> sizeof

# ╔═╡ bcac20a8-734b-4522-a7ba-54a3ee3afe4e
md"""
# Visual alignment of reads
"""

# ╔═╡ b25b103c-01a2-4910-91d3-df0e36d6f896
# tbd..

# ╔═╡ Cell order:
# ╠═729fd529-ddfc-4d0a-b312-3756ad7e6a92
# ╠═8cb18ea8-50dd-11ec-1fa5-a9f0dceeaf77
# ╠═8adc5b1f-2703-433c-9b78-3fe9131ccedb
# ╠═84e07b02-86b0-4d5d-ba1f-48323b10af02
# ╠═e69b6656-9323-4f07-8ec8-2f52b313f0d6
# ╠═96f976aa-415f-42a1-925d-f9ff10ecdd2c
# ╟─b4b48f33-53f1-4989-bcd6-8ed87c18de49
# ╠═5c330e6b-4aa3-4860-874d-b2b69d70eb47
# ╟─3bc8b9e7-af20-4e22-adaf-0dc9016743c8
# ╠═46e17e31-9fde-4610-b5ac-85087487a5c5
# ╠═950dbe45-68dd-4a07-87c5-03caf3c9c8cf
# ╟─03e8865b-33a3-47e7-8ffd-fdbe6e289758
# ╠═ca4e1b48-ea73-4934-8e8b-3db5ccc7c1aa
# ╠═c8a54abe-af83-4816-921e-9e4222f8bc4a
# ╠═4f43c2a5-49b8-4295-a262-3ccebfc0c4d9
# ╠═440f3f60-a611-4a42-be96-7a98333cff2f
# ╠═e50466f0-863c-4ea6-927f-993838a629c3
# ╠═d83273c3-ed36-4f18-b07c-a024dc9bf265
# ╠═585433d8-b8ee-41c3-80d9-42080859a6cb
# ╠═5510c22f-3a17-47cb-b1f3-78192b57c56f
# ╠═c7090264-0874-422c-ab2f-cb76f4135433
# ╠═bcac20a8-734b-4522-a7ba-54a3ee3afe4e
# ╠═b25b103c-01a2-4910-91d3-df0e36d6f896
